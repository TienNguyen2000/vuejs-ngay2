Vue.filter('capitalize', function(value){
    if (!value) return ''
    value = value.toString().toLowerCase();
    return value.charAt(0).toUpperCase() + value.slice(1)
})
var app = new Vue({
    el: "#app",
    data: {
        message: ''
    },
    filters: {
        toUpperCase(message){
            return message.toUpperCase();
        },
        toLowerCase(message){
            return message.toLowerCase();
        }
    }
});