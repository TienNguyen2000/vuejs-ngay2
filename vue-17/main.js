var myMixin = {
    created: function() {
        this.hello()
    },
    methods: {
        hello: function() {
            console.log('hello from mixin!');
        }
    }
}
var mixin = {
    data: function() {
        return {
            message: 'Hello World',
            foo: 'abc'
        }
    }
}
var Component = Vue.extend({
    mixins: [myMixin]
})
var component = new Component();
var app = new Vue({
    mixins: [mixin],
    el: "#app",
    data: {

    }
});